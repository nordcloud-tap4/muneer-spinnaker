provider "google" {
  version = "~> 2.5.1"
  project = "${var.project}"
}

provider "google-beta" {
  version = "~> 2.5.1"
  project = "${var.project}"
}

# serviceusage.googleapis.com has to be enabled manually
resource "google_project_service" "project_service_usage" {
  service = "serviceusage.googleapis.com"
  disable_dependent_services = true
}

resource "google_project_service" "project_service_iam" {
  service = "iam.googleapis.com"
  disable_dependent_services = true
}

resource "google_project_service" "project_service_cloudresource" {
  service = "cloudresourcemanager.googleapis.com"
  disable_dependent_services = true
}

resource "google_project_service" "project_service_compute" {
  service = "compute.googleapis.com"
  disable_dependent_services = true
}

resource "google_project_service" "project_service_pubsub" {
  service = "pubsub.googleapis.com"
  disable_dependent_services = true
}

resource "google_project_service" "project_service_container" {
  service = "container.googleapis.com"
  disable_dependent_services = true
}

#module "vm_bastion" {
#  source = "modules/bastion"
#  region = "${var.default_region}"
#  zone   = "${var.default_zone}"

  # depends_on = ["google_project_services.myproject"]

  ## Project id is added as tag to create dependency between project service and this module
#  project = "${var.project}"
#}

module "vm_spinnaker" {
  source = "modules/spinnaker"
  region = "${var.default_region}"
  zone   = "${var.default_zone}"

  # depends_on = ["google_project_services.myproject"]

  ## Project id is added as tag to create dependency between project service and this module
  project = "${var.project}"
}
