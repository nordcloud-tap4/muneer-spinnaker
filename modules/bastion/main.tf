resource "google_service_account" "bastion_service_account" {
  account_id   = "${var.service_account}"
  display_name = "${var.service_account}"
}

resource "google_service_account_iam_binding" "bastion_service_account_service_account_user" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/iam.serviceAccountUser"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_service_account_creator" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/iam.serviceAccountCreator"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

#resource "google_service_account_iam_binding" "bastion_service_account_service_usage_admin" {
#  service_account_id = "${google_service_account.bastion_service_account.name}"
#  role               = "roles/iam.serviceUsageAdmin"
#  members            = [
#    "serviceAccount:${google_service_account.bastion_service_account.email}"
#  ]
#}
#
#resource "google_service_account_iam_binding" "bastion_service_account_project_iam_admin" {
#  service_account_id = "${google_service_account.bastion_service_account.name}"
#  role               = "roles/iam.projectIamAdmin"
#  members            = [
#    "serviceAccount:${google_service_account.bastion_service_account.email}"
#  ]
#}

resource "google_service_account_iam_binding" "bastion_service_account_service_account_key_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/iam.serviceAccountKeyAdmin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

#resource "google_service_account_iam_binding" "bastion_service_account_pubsub_admin" {
#  service_account_id = "${google_service_account.bastion_service_account.name}"
#  role               = "roles/pubsub.admin"
#  members            = [
#    "serviceAccount:${google_service_account.bastion_service_account.email}"
#  ]
#}
#
#resource "google_service_account_iam_binding" "bastion_service_account_container_cluster_admin" {
#  service_account_id = "${google_service_account.bastion_service_account.name}"
#  role               = "roles/container.clusterAdmin"
#  members            = [
#    "serviceAccount:${google_service_account.bastion_service_account.email}"
#  ]
#}
#
#resource "google_service_account_iam_binding" "bastion_service_account_container_admin" {
#  service_account_id = "${google_service_account.bastion_service_account.name}"
#  role               = "roles/container.admin"
#  members            = [
#    "serviceAccount:${google_service_account.bastion_service_account.email}"
#  ]
#}
#
#resource "google_service_account_iam_binding" "bastion_service_account_storage_admin" {
#  service_account_id = "${google_service_account.bastion_service_account.name}"
#  role               = "roles/storage.admin"
#  members            = [
#    "serviceAccount:${google_service_account.bastion_service_account.email}"
#  ]
#}

data "template_file" "bastion_vm_init" {
  template = <<EOF
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install google-cloud-sdk
sudo apt-get install kubectl
sudo apt-get install unzip
sudo apt-get install wget
wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip terraform_0.11.13_linux_amd64.zip
sudo mv terraform /usr/local/bin/
wget https://storage.googleapis.com/kubernetes-helm/helm-v2.10.0-linux-amd64.tar.gz
tar zxfv helm-v2.10.0-linux-amd64.tar.gz
sudo cp linux-amd64/helm /usr/local/bin/
EOF
}

resource "google_compute_instance" "bastion" {
  name         = "${var.bastion_vm}"
  machine_type = "${var.bastion_vm_machine}"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "${var.bastion_vm_image}"
    }
  }

  network_interface {
    network = "${var.vpc}"
    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "${data.template_file.bastion_vm_init.rendered}"

  service_account {
    email  = "${google_service_account.bastion_service_account.email}"
    scopes = ["cloud-platform"]
  }
}
