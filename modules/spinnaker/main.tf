resource "google_container_cluster" "primary" {
  name               = "${var.spinnaker_cluster_name}"
  project            = "${var.project}"
  location           = "${var.zone}"

  node_pool {

    node_config {
      machine_type = "${var.machine_type}"
    }
  }
}

resource "google_service_account" "spinnaker_service_account" {
  account_id   = "${var.service_account}"
  display_name = "${var.service_account}"
}

## google_service_account_iam_binding is not working for some roles including below
#resource "google_service_account_iam_binding" "spinnaker_service_account_storage_admin" {
#  service_account_id = "${google_service_account.spinnaker_service_account.name}"
#  role               = "roles/storage.admin"
#  members            = [
#    "serviceAccount:${google_service_account.spinnaker_service_account.email}"
#  ]
#}

data "template_file" "spinnaker_service_account_roles" {
  template = <<EOF
gcloud projects add-iam-policy-binding $${project}\
 --member serviceAccount:$${sa_email}\
 --role roles/storage.admin
EOF

  vars {
    project  = "${var.project}"
    sa_email = "${google_service_account.spinnaker_service_account.email}"
  }
}

resource "null_resource" "spinnaker_service_account_roles" {
  provisioner "local-exec" {
    command = "${data.template_file.spinnaker_service_account_roles.rendered}"
  }
}

## How to get private key
#resource "google_service_account_key" "spinnaker_service_account" {
#  service_account_id = "${google_service_account.spinnaker_service_account.name}"
#}

data "template_file" "spinnaker_service_account_key" {
  template = <<EOF
gcloud iam service-accounts keys create $${sa_key_file} --iam-account $${sa_email}
EOF

  vars {
    project     = "${var.project}"
    sa_email    = "${google_service_account.spinnaker_service_account.email}"
	  sa_key_file = "${var.service_account_key}"
  }
}

resource "null_resource" "spinnaker_service_account_key" {
  provisioner "local-exec" {
    command = "${data.template_file.spinnaker_service_account_key.rendered}"
  }
}

resource "google_pubsub_topic" "my_topic" {
  name    = "gcr"
  project = "${var.project}"
}

resource "google_pubsub_subscription" "my_topic_subscr" {
  name    = "gcr-triggers"
  project = "${var.project}"
  topic   = "${google_pubsub_topic.my_topic.name}"
}

resource "google_pubsub_subscription_iam_binding" "my_topic_subscr_iam" {
  subscription = "${google_pubsub_subscription.my_topic_subscr.name}"
  project      = "${var.project}"
  role         = "roles/pubsub.subscriber"
  members      = [
    "serviceAccount:${google_service_account.spinnaker_service_account.email}",
  ]
}

data "template_file" "kube_config" {
  template = <<EOF
gcloud container clusters get-credentials $${cluster_name} --zone=$${zone}
EOF

  vars {
    project      = "${var.project}"
    zone         = "${var.zone}"
    cluster_name = "${var.spinnaker_cluster_name}"
  }
}

resource "null_resource" "kube_config" {
  provisioner "local-exec" {
    command = "${data.template_file.kube_config.rendered}"
  }
}

resource "kubernetes_cluster_role_binding" "user_admin_binding" {
  metadata {
    name = "user-admin-binding"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "User"
    name      = "${google_service_account.spinnaker_service_account.name}"
    api_group = "rbac.authorization.k8s.io"
  }
  depends_on = ["null_resource.kube_config"]
}

resource "kubernetes_service_account" "tiller_service_account" {
  metadata {
    name      = "tiller"
    namespace = "kube-system"
  }
  depends_on = ["null_resource.kube_config"]
}

resource "kubernetes_cluster_role_binding" "tiller_admin_binding" {
  metadata {
    name = "tiller-admin-binding"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "tiller"
    namespace = "kube-system"
  }
  depends_on = ["null_resource.kube_config", "kubernetes_service_account.tiller_service_account"]
}

resource "kubernetes_cluster_role_binding" "spinnaker_admin" {
  metadata {
    name = "spinnaker-admin"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "default"
  }
  depends_on = ["null_resource.kube_config"]
}
